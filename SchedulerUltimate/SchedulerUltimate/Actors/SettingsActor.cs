﻿using System.IO;
using Akka.Actor;
using Newtonsoft.Json;
using SchedulerUltimate.Messages;
using SchedulerUltimate.Model;
using Serilog;

namespace SchedulerUltimate.Actors
{
    public class SettingsActor : ReceiveActor
    {
        private FileReadingSettings _settings;
        private readonly IActorRef _mailConfigurationActor;

        public SettingsActor()
        {
            _mailConfigurationActor = Context.ActorOf<MailConfigurationActor>();

            Receive<GetSettingsMessage>(_ => GetSettings());
            Receive<UpdateReadRecordsMessage>(msg => UpdateReadRecords(msg));
            Receive<GetMailConfigurationMessage>(msg => GetMailConfiguration(msg));
        }

        private void UpdateReadRecords(UpdateReadRecordsMessage message)
        {
            _settings.LastRecordRead += message.CurrentReadRecords;
            Log.Information($"Total read records: {_settings.LastRecordRead}");

            var jsonFile = JsonConvert.SerializeObject(_settings);
            File.WriteAllText("FileReadingSettings.json", jsonFile);


            //Sender.Tell(_settings.LastRecordRead, Self);
        }
        private void GetSettings()
        {
            if (_settings == null)
            {
                var jsonFile = File.ReadAllText("FileReadingSettings.json");
                _settings = JsonConvert.DeserializeObject<FileReadingSettings>(jsonFile);
            }
            Sender.Tell(_settings, Self);
        }

        private void GetMailConfiguration(GetMailConfigurationMessage mailConfiguration)
        {
            _mailConfigurationActor.Forward(mailConfiguration);
        }
    }
}
