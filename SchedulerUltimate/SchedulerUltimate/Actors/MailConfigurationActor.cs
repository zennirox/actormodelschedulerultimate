﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using Akka.Actor;
using Newtonsoft.Json;
using SchedulerUltimate.Messages;
using SchedulerUltimate.Model;
using Serilog;

namespace SchedulerUltimate.Actors
{
    internal class MailConfigurationActor : ReceiveActor
    {
        private SmtpClient _smtpClient;
        public MailConfigurationActor()
        {
            Receive<GetMailConfigurationMessage> (_ => GetMailConfiguration());
        }

        private void GetMailConfiguration()
        {
            if (_smtpClient != null)
            {
                Sender.Tell(_smtpClient, Self);
                return;
            }

            try
            {
                var jsonFile = File.ReadAllText("EmailConfiguration.json");
                var configuration = JsonConvert.DeserializeObject<MailConfiguration>(jsonFile);
                var client = new SmtpClient(configuration.Smtp, configuration.Port)
                {
                    UseDefaultCredentials = false,
                    EnableSsl = true,
                    Credentials = new NetworkCredential(configuration.Email, configuration.Password)
                };
                _smtpClient = client;
                Sender.Tell(client, Self);
            }
            catch (Exception e)
            {
                Log.Error(e, "Failed to read and set mail configuration");
                throw;
            }
            
        }

    }
}
