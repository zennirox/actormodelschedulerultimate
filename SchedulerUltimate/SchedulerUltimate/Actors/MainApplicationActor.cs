﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Akka.Actor;
using SchedulerUltimate.Messages;
using SchedulerUltimate.Model;
using Serilog;

namespace SchedulerUltimate.Actors
{
    public class MainApplicationActor : ReceiveActor
    {
        private readonly IActorRef _settingsActor;
        public MainApplicationActor()
        {
            Receive<ScheduleMessage>(x => Handle(x));
            _settingsActor = Context.ActorOf<SettingsActor>();
        }
        private void Handle(ScheduleMessage scheduleMessage)
        {

            var readRecords = Context
                .ActorOf<ReadFileActor>()
                .Ask<ICollection<CsvRecord>>(new ReadFileMessage(
                    _settingsActor
                        .Ask<FileReadingSettings>(new GetSettingsMessage())
                        .Result
                ))
                .Result;

            if (!readRecords.Any())
            {
                Log.Information("No new records found");
                return;
            }

            var validReadRecords = readRecords
                .Where(
                    r => Context
                        .ActorOf<CsvRecordValidateActor>()
                        .Ask<bool>(r)
                        .Result
                )
                .ToArray();

            var preparedMails = validReadRecords
                .Select(
                    r => Context
                        .ActorOf<MailContextGeneratorActor>()
                        .Ask<MailContext>(new CreateMailContextMessage(r))
                        .Result
                )
                .ToArray();

            var client = _settingsActor.Ask<SmtpClient>(new GetMailConfigurationMessage()).Result;

            foreach (var preparedMail in preparedMails)
            {
                var success = Context.ActorOf<MailSendingActor>()
                    .Ask<bool>(new SendMailMessage(client, preparedMail)).Result;

                if (!success)
                {
                    Context.ActorOf<MailSendingActor>()
                        .Ask<bool>(new SendMailMessage(client, preparedMail)).Wait();
                }
            }

            //Ta wersja na 100% jest ok bo czekamy na wykonanie taska więc zawsze
            //prawidłowo zwiększe wartość
            //var currentValue = _settingsActor
            //    .Ask<int>(
            //        new UpdateReadRecordsMessage(getRecordTask.Count))
            //    .Result;
            //Log.Information($"Total read records: {currentValue}");

            //czemu działa, jesli to jest zapytanie async,
            //zawsze dobrze zwiększa wartośc?
            //tutaj prawdopodobnie aktor przetwarzając to zapytanie nie może
            //przetwarzać jednoczenie zapytania o ustawienia z linijki 24 w tym pliku
            _settingsActor.Tell(
                new UpdateReadRecordsMessage(readRecords.Count),
                Self
                );
        }
    }
}
