﻿using System;
using Akka.Actor;
using SchedulerUltimate.Model;

namespace SchedulerUltimate.Actors
{
    public class CsvRecordValidateActor : ReceiveActor
    {
        public CsvRecordValidateActor()
        {
            Receive<CsvRecord>(record => ValidateRecord(record));
        }

        private void ValidateRecord(CsvRecord csvRecord)
        {
            Sender.Tell(csvRecord.DiscountExpire > DateTime.Now, Self);
        }
    }
}
