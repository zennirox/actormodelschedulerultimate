﻿using System;
using Akka.Actor;
using SchedulerUltimate.Messages;

namespace SchedulerUltimate.Actors
{
    internal class SchedulerActor : ReceiveActor
    {
        private readonly int _secondsDelay;
        private ICancelable _cancelable;

        public SchedulerActor(int secondsDelay)
        {
            _secondsDelay = secondsDelay;
        }
        protected override void PreStart()
        {
            _cancelable = Context.System.Scheduler.ScheduleTellRepeatedlyCancelable(
                TimeSpan.Zero,
                TimeSpan.FromSeconds(_secondsDelay),
                Context.ActorOf<MainApplicationActor>(),
                new ScheduleMessage(),
                Self
                );
        }

        public static Props Props(int secondsDelay)
        {
            return Akka.Actor.Props.Create(() => new SchedulerActor(secondsDelay));
        }
        protected override void PostStop()
        {
            _cancelable.Cancel();
        }
    }
}
