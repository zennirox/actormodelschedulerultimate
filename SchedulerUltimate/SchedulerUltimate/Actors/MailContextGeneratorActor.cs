﻿using Akka.Actor;
using SchedulerUltimate.Messages;
using SchedulerUltimate.Model;

namespace SchedulerUltimate.Actors
{
    public class MailContextGeneratorActor : ReceiveActor
    {
        public MailContextGeneratorActor()
        {
            Receive<CreateMailContextMessage>(x => GenerateMailContext(x));
        }

        private void GenerateMailContext(CreateMailContextMessage contextMessage)
        {
            var record = contextMessage.CsvRecord;
            var mailContext = new MailContext("SPAM BOT", record.Address, "Special Offer",
                $@"Hello my {record.Name} {record.Surname}.
Special discount: {record.Discount} is available to {record.DiscountExpire}");

            Sender.Tell(mailContext, Self);
        }
    }
}
