﻿using System;
using Akka.Actor;
using FluentEmail.Core;
using FluentEmail.Smtp;
using SchedulerUltimate.Messages;
using Serilog;

namespace SchedulerUltimate.Actors
{
    public class MailSendingActor : ReceiveActor
    {
        public MailSendingActor()
        {
            Receive<SendMailMessage>(msg => SendMail(msg));
        }

        private void SendMail(SendMailMessage mailMessage)
        {
            try
            {
                //var sender = new SmtpSender(mailMessage.Client);
                var information = mailMessage.Context;

                Log.Information($"Prepare to send email: {information}");

                var email = Email.From(information.From)
                    .To(information.To)
                    .Subject(information.Subject)
                    .Body(information.Context);

                Log.Information("Sending...");
                //sender.Send(email);
                Log.Information("Mail sent correct.");

                Sender.Tell(true, Self);
            }
            catch (Exception e)
            {
                Log.Error(e, "Failed to sent mail");
                Sender.Tell(false, Self);
            }
        }
    }
}
