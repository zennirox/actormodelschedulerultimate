﻿using System;
using System.IO;
using System.Linq;
using Akka.Actor;
using CsvHelper;
using SchedulerUltimate.Messages;
using SchedulerUltimate.Model;

namespace SchedulerUltimate.Actors
{
    public class ReadFileActor : ReceiveActor
    {
        public ReadFileActor()
        {
            Receive<ReadFileMessage>(msg => ReadFile(msg));
        }

        private void ReadFile(ReadFileMessage message)
        {
            using (var reader = new CsvReader(File.OpenText(message.PathToFile)))
            {
                if (message.MaxRecordsToRead < 0)
                {
                    throw new ArgumentOutOfRangeException(
                        $"{nameof(message.MaxRecordsToRead)} has value " +
                        $"{message.MaxRecordsToRead} when min value is 0"
                    );
                }

                var records = reader.GetRecords<CsvRecord>()
                    .Skip(message.LastReadRecord)
                    .Take(message.MaxRecordsToRead)
                    .ToArray();

                Sender.Tell(records, Self);
            }
        }
    }

}
