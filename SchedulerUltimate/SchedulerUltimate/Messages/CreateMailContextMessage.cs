﻿using SchedulerUltimate.Model;

namespace SchedulerUltimate.Messages
{
    public class CreateMailContextMessage
    {
        public CsvRecord CsvRecord { get; }

        public CreateMailContextMessage(CsvRecord csvRecord)
        {
            CsvRecord = csvRecord;
        }
    }
}
