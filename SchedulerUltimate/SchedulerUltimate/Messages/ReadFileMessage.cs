﻿using SchedulerUltimate.Model;

namespace SchedulerUltimate.Messages
{
    public class ReadFileMessage
    {
        public string PathToFile { get; }
        public int MaxRecordsToRead { get; }
        public int LastReadRecord { get; }

        public ReadFileMessage(string pathToFile, int maxRecordsToRead, int lastReadRecord)
        {
            PathToFile = pathToFile;
            MaxRecordsToRead = maxRecordsToRead;
            LastReadRecord = lastReadRecord;
        }

        public ReadFileMessage(FileReadingSettings fileReadingSettings)
        {
            PathToFile = fileReadingSettings.Path;
            MaxRecordsToRead = fileReadingSettings.MaxRecordsToRead;
            LastReadRecord = fileReadingSettings.LastRecordRead;
        }
    }
}
