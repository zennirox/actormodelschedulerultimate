﻿namespace SchedulerUltimate.Messages
{
    public class UpdateReadRecordsMessage
    {
        public int CurrentReadRecords { get; }

        public UpdateReadRecordsMessage(int currentReadRecords)
        {
            CurrentReadRecords = currentReadRecords;
        }
    }
}
