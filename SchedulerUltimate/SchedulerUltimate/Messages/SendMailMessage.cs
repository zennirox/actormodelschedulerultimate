﻿using System.Net.Mail;
using SchedulerUltimate.Model;

namespace SchedulerUltimate.Messages
{
    public class SendMailMessage
    {
        public SmtpClient Client { get; }
        public MailContext Context { get; }

        public SendMailMessage(SmtpClient client, MailContext context)
        {
            Client = client;
            Context = context;
        }

    }
}
