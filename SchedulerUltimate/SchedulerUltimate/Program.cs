﻿using System;
using Akka.Actor;
using SchedulerUltimate.Actors;
using Serilog;
using Serilog.Events;

namespace SchedulerUltimate
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .CreateLogger();

            var system = ActorSystem.Create("ActorDialogue");
            system.ActorOf(SchedulerActor.Props(5),"scheduler");
            Console.ReadLine();

            system
                .Terminate()
                .ContinueWith(x => Console.WriteLine("Program finish"));

            system.WhenTerminated.Wait();
            Console.ReadLine();
        }
    }
}
