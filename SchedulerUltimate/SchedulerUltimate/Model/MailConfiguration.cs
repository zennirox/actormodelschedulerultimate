﻿using System;

namespace SchedulerUltimate.Model
{
    public class MailConfiguration
    {
        public string Email { get; }
        public string Password { get; }
        public string Smtp { get; }
        public int Port { get; }

        public MailConfiguration(string email, string password, string smtp, int port)
        {
            if (port <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(port));
            }
            Email = email;
            Password = password;
            Smtp = smtp;
            Port = port;
        }
    }
}