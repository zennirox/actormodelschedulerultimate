﻿namespace SchedulerUltimate.Model
{
    public class FileReadingSettings
    {
        public string Path { get; }
        public int MaxRecordsToRead { get; }
        public int LastRecordRead { get; set; }
        public FileReadingSettings(string path, int maxRecordsToRead)
        {
            Path = path;
            MaxRecordsToRead = maxRecordsToRead;
        }
    }
}
